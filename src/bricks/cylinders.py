import mujoco as mj
import numpy as np
from mujoco.glfw import glfw

from bricks.mujoco_base import MuJoCoBase


class Cylinders(MuJoCoBase):
    def __init__(self, xml_path):
        super().__init__(xml_path)
        self.simend = 100.0
        self.ticked = False

    def reset(self):
        self.data.qvel[5] = 0
        self.data.qvel[6] = 0
        
        # Set camera configuration
        self.cam.azimuth = 89.608063
        self.cam.elevation = -11.588379
        self.cam.distance = 6.0
        self.cam.lookat = np.array([0.0, 0.0, 2.0])

        mj.set_mjcb_control(self.controller)

    def controller(self, model, data):
        pass

    def simulate(self):
        while not glfw.window_should_close(self.window):
            simstart = self.data.time
            while (self.data.time - simstart < 1.0/60.0):
                mj.mj_step(self.model, self.data)

            if self.data.time >= self.simend:
                break
            
            # Move wheel for one simulation tick
            if not self.ticked and self.data.time >= 2.0:
                self.data.ctrl = 10
                self.ticked = True
            else:
                self.data.ctrl = 0

            viewport_width, viewport_height = glfw.get_framebuffer_size(
                self.window)
            viewport = mj.MjrRect(0, 0, viewport_width, viewport_height)

            mj.mjv_updateScene(self.model, self.data, self.opt, None, self.cam,
                               mj.mjtCatBit.mjCAT_ALL.value, self.scene)
            mj.mjr_render(viewport, self.scene, self.context)

            glfw.swap_buffers(self.window)
            glfw.poll_events()

        glfw.terminate()


def main():
    xml_path = "./xml/cylinders.xml"
    sim = Cylinders(xml_path)
    sim.reset()
    sim.simulate()


if __name__ == "__main__":
    main()
# Instalation

Install [Poetry](https://python-poetry.org/docs/#installing-with-the-official-installer).

Then run:
```
# poetry env use python3.11
# poetry install
# poetry shell
(venv) # python3 src/bricks/triple_pendulum.py
```